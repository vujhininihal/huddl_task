import {
  Button,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  withStyles
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

const styles = theme => ({
  text: {
    padding: "10px"
  },
  button: {
    float: "right",
    padding: "5px"
  }
});
class IndividualPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      postId: 0,
      userName: "",
      postTitle: ""
    };
  }

  componentDidMount() {
    this.setState({
      postId: this.props.location.state
        ? this.props.location.state.postId
        : null,
      userName: this.props.location.state
        ? this.props.location.state.userName
        : null,
      postTitle: this.props.location.state
        ? this.props.location.state.postTitle
        : null
    });
    fetch("http://jsonplaceholder.typicode.com/comments")
      .then(res => res.json())
      .then(data => {
        this.setState({ comments: data });
      });
  }

  render() {
    const { comments, userName, postId, postTitle } = this.state;
    const { classes } = this.props;
    return (
      <Fragment>
        <div>
          <Typography variant="h5"> Comments related to the post </Typography>
          <Typography>
            {" "}
            Username of post creator "{userName}" and Title of the post "
            {postTitle}"
          </Typography>
          {comments.map(
            (comment, index) =>
              comment.postId === postId && (
                <ExpansionPanel>
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography>Comment ID: {comment.id} </Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails style={{ flexDirection: "column" }}>
                    <Typography variant="h6" className={classes.text}>
                      Subject of Comments :{comment.name}
                    </Typography>
                    <Typography variant="h6" className={classes.text}>
                      Comment Body : {comment.body}
                    </Typography>
                    <Typography variant="h6" className={classes.text}>
                      {" "}
                      Email : {comment.email}
                    </Typography>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              )
          )}
        </div>
        <div>
          <Link to={{ pathname: "/post" }} className={classes.button}>
            <Button variant="contained">BACK</Button>
          </Link>
        </div>
      </Fragment>
    );
  }
}
export default withStyles(styles)(IndividualPost);
