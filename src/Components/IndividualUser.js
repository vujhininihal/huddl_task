import React, { Component, Fragment } from "react";
import { Button, Grid, Typography, withStyles } from "@material-ui/core";
import { Link } from "react-router-dom";

const styles = theme => ({
  text: {
    padding: "10px"
  },
  button: {
    float: "right",
    padding: "5px"
  }
});

class IndividualUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  componentDidMount() {
    this.props.location.state &&
      this.setState({
        users: this.props.location.state.users[
          this.props.location.state.userId - 1
        ]
      });
  }

  render() {
    const { users } = this.state;
    const { classes } = this.props;
    return (
      <Fragment>
        <div>
          <Typography variant="h4"> SELECTED USER INFORMATION </Typography>
          <Grid container>
            <Grid item md={4}>
              {" "}
              <Typography className={classes.text}>
                {" "}
                FULL NAME: {users.name}
              </Typography>{" "}
            </Grid>
            <Grid item md={4}>
              {" "}
              <Typography className={classes.text}>
                USER NAME : {users.username}
              </Typography>{" "}
            </Grid>
          </Grid>
          <Grid container>
            <Grid item md={4}>
              {" "}
              <Typography className={classes.text}>
                Email ID : {users.email}
              </Typography>{" "}
            </Grid>
            <Grid item md={4}>
              {" "}
              <Typography className={classes.text}>
                Website : {users.website}
              </Typography>{" "}
            </Grid>
          </Grid>
          <Typography className={classes.text} variant="h5">
            Company Details
          </Typography>
          <Typography className={classes.text}>
            Company Name : {users.company ? users.company.name : ""}
          </Typography>
          <Typography className={classes.text}>
            company catchPhase :{" "}
            {users.company ? users.company.catchPhrase : ""}
          </Typography>
          <Typography className={classes.text}>
            Company bs : {users.company ? users.company.bs : ""}
          </Typography>
        </div>
        <div>
          <div>
            <Link to={{ pathname: "/post" }} className={classes.button}>
              <Button variant="contained">BACK</Button>
            </Link>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default withStyles(styles)(IndividualUser);
