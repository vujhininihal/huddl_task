import React, { Component, Fragment } from "react";
import { Grid, Typography, withStyles } from "@material-ui/core";
import { Link } from "react-router-dom";

const styles = theme => ({
  search: {
    float: "right"
  },
  textDecoration: {
    textDecoration: "none"
  },
  textStyle: {
    textAlignLast: "center",
    color: "black",
    padding: "5px"
  }
});

class PostList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      users: [],
      searchString: ""
    };
  }

  componentDidMount() {
    fetch("http://jsonplaceholder.typicode.com/posts")
      .then(res => res.json())
      .then(data => {
        this.setState({ posts: data });
      })
      .catch(console.log);
    fetch("http://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(data => {
        this.setState({ users: data });
      });
  }
  handleChange = e => {
    this.setState({ searchString: e.target.value });
  };

  render() {
    var filteredUsers = this.state.users;
    var searchString = this.state.searchString.trim().toLowerCase();
    if (searchString.length > 0) {
      filteredUsers = filteredUsers.filter(function(user) {
        return user.username.toLowerCase().match(searchString);
      });
    }
    const { users, posts } = this.state;
    const { classes } = this.props;
    return (
      <Fragment>
        <div className={classes.search}>
          <input
            type="text"
            value={this.state.searchString}
            ref="search"
            onChange={this.handleChange}
            placeholder="type name here"
          />
        </div>
        {filteredUsers.map(user => (
          <div>
            <Grid key={user.id} container>
              <Grid item md={12}>
                <Link
                  to={{
                    pathname: "/user",
                    state: { users, userId: user.id }
                  }}
                  className={classes.textDecoration}
                >
                  <Typography className={classes.textStyle} variant="h4">
                    Username of post creator : {user.username}{" "}
                  </Typography>
                </Link>
              </Grid>
              <Grid item md={12}>
                <Typography variant="h5">Title of posts</Typography>
              </Grid>
              {posts.map(
                post =>
                  user.id === post.userId && (
                    <Grid item md={6}>
                      <Link
                        to={{
                          pathname: "/comments",
                          state: {
                            postId: post.id,
                            userName: user.username,
                            postTitle: post.title
                          }
                        }}
                        className={classes.textDecoration}
                      >
                        <Typography variant="h6"> ->{post.title}</Typography>{" "}
                      </Link>
                    </Grid>
                  )
              )}
            </Grid>
          </div>
        ))}
      </Fragment>
    );
  }
}
export default withStyles(styles)(PostList);
