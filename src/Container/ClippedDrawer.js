import {
  AppBar,
  Drawer,
  List,
  Toolbar,
  Typography,
  withStyles
} from "@material-ui/core";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { Route } from "react-router-dom";

import { Items } from "./tileData";
import PostList from "../Components/PostList";
import IndividualUser from "../Components/IndividualUser";
import IndividualPost from "../Components/IndividualPost";

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "100%",
    zIndex: 1,
    overflow: "hidden",
    position: "relative",
    display: "flex"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  drawerPaper: {
    position: "relative",
    width: 170
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0,
    top: 0
  },
  toolbar: theme.mixins.toolbar
});
class ClippedDrawer extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="absolute" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" color="inherit" noWrap>
              Huddl Enterprise Communication Pvt. Ltd.
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List>{Items}</List>
        </Drawer>

        <main className={classes.content}>
          <div className={classes.toolbar} />
          <div>
            <Route exact path="/post" component={PostList} />
            <Route exact path="/user" component={IndividualUser} />
            <Route exact path="/comments" component={IndividualPost} />
          </div>
        </main>
      </div>
    );
  }
}
ClippedDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ClippedDrawer);
