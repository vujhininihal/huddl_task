import React from "react";
import { BrowserRouter, Switch,} from "react-router-dom";
import ClippedDrawer from './Container/ClippedDrawer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <BrowserRouter>
          <Switch>
    <ClippedDrawer />
          </Switch>
        </BrowserRouter>
      </header>
    </div>
  );
}

export default App;